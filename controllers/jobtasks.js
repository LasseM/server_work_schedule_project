const Jobtasks = require('../models/jobtasks.js')

//=====================================================
const add = async (req, res) => {
    
    try{

    const response = await Jobtasks.create({

        jobtaskID: req.body.jobtaskID,
        jobtask_type: req.body.jobtask_type,
        jobtask_image: req.body.jobtask_image,
        jobtask_logo: req.body.jobtask_logo,
        jobtask_firstname: req.body.jobtask_firstname,
        jobtask_middlename: req.body.jobtask_middlename,
        jobtask_lastname: req.body.jobtask_lastname,
        jobtask_date: req.body.jobtask_date,
        jobtask_startdate: req.body.jobtask_startdate,
        jobtask_starttime: req.body.jobtask_starttime,
        jobtask_enddate: req.body.jobtask_enddate,
        jobtask_endtime: req.body.jobtask_endtime,
        jobtask_country_location: req.body.jobtask_country_location,
        jobtask_city_location: req.body.jobtask_city_location,
        jobtask_location: req.body.jobtask_location,
        jobtask_adress_street: req.body.jobtask_adress_street,
        jobtask_adress_number: req.body.jobtask_adress_number,
        jobtask_adress_postcode: req.body.jobtask_adress_postcode,
        jobtask_adress_country: req.body.jobtask_adress_country,
        jobtask_confirmed: req.body.jobtask_confirmed,
        jobtask_language: req.body.jobtask_language,
        jobtask_general_notes_to_staff: req.body.jobtask_general_notes_to_staff,
        jobtask_dutymanagers: req.body.jobtask_dutymanagers,
            
    })

    console.log(response)
    res.send({ok: true, message: `${response.jobtask_firstname} created in jobtasks collection`})

    }catch(error){

        console.log(error)
        res.send({ok: false, message: `Something went wrong`})
    }
}
// //=====================================================

const remove = async (req, res) => {
    try {
        console.log(req.params)
        const { _id } = req.params
        const response = await Jobtasks.deleteOne({_id: _id})
        console.log(response)
        res.send({ok: true, message:`message from server jobtask removed`})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Could not find unique id - ObjectId failed'})
    }
}

// // //=====================================================

const findjobtasks = async (req, res) => {
 
    try {
        const response = await Jobtasks.find({})
        
        res.send({ok: true, response})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}

// // //=====================================================

const findOnejobtask = async (req, res) => {
   
    const { _id, jobtask_firstname } = req.body
    const value = _id || jobtask_firstname
    const key = _id? "_id": "jobtask_firstname"
 
    try {
        const response = await Jobtasks.findOne({[key]:value})
        if(response == null) {
            console.log('response ====>', res)
            res.send({ok: false, message: `${value} does not exist`})
        }
        res.send({ok: true, response})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}
// // //=====================================================

const updatejobtask = async (req, res) => {
    
   const { _id, 
    jobtaskID,
    jobtask_type,
    jobtask_image,
    jobtask_logo,
    jobtask_firstname,
    jobtask_middlename,
    jobtask_lastname,
    jobtask_date,
    jobtask_startdate,
    jobtask_starttime,
    jobtask_enddate,
    jobtask_endtime,
    jobtask_country_location,
    jobtask_city_location,
    jobtask_location,
    jobtask_adress_street,
    jobtask_adress_number,
    jobtask_adress_postcode,
    jobtask_adress_country,
    jobtask_staffneeded,
    jobtask_type_of_staff_needed,
    jobtask_staffsignedup,
    jobtask_language,
    jobtask_general_notes_to_staff,
    jobtask_dutymanagers,
        } = req.body
    try {
        const response = await Jobtasks.updateOne({_id:_id}, 
            {
        jobtaskID: jobtaskID,
        jobtask_type: jobtask_type,
        jobtask_image: jobtask_image,
        jobtask_logo: jobtask_logo,
        jobtask_firstname: jobtask_firstname,
        jobtask_middlename: jobtask_middlename,
        jobtask_lastname: jobtask_lastname,
        jobtask_date: jobtask_date,
        jobtask_startdate: jobtask_startdate,
        jobtask_starttime: jobtask_starttime,
        jobtask_enddate: jobtask_enddate,
        jobtask_endtime: jobtask_endtime,
        jobtask_country_location: jobtask_country_location,
        jobtask_city_location: jobtask_city_location,
        jobtask_location: jobtask_location,
        jobtask_adress_street: jobtask_adress_street,
        jobtask_adress_number: jobtask_adress_number,
        jobtask_adress_postcode: jobtask_adress_postcode,
        jobtask_adress_country: jobtask_adress_country,
        jobtask_staffneeded: jobtask_staffneeded,
        jobtask_type_of_staff_needed: jobtask_type_of_staff_needed,
        jobtask_staffsignedup: jobtask_staffsignedup,
        jobtask_language: jobtask_language,
        jobtask_general_notes_to_staff: jobtask_general_notes_to_staff,
        jobtask_dutymanagers: jobtask_dutymanagers,
            })
        console.log(response)      
        res.send({ok: true, response, Updatedjobtask:req.body})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}


module.exports = {
    add,
    remove,
    findjobtasks,
    findOnejobtask,
    updatejobtask

}