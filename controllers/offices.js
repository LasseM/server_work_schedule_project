const Offices = require('../models/offices.js')

//=====================================================
const add = async (req, res) => {
    
    try{

    const response = await Offices.create({

        officeID: req.body.officeID,
        
        officeID: req.body.officeID,
        office_image: req.body.office_image,
        office_logo: req.body.office_logo,
        office_firstname: req.body.office_firstname,
        office_middlename: req.body.office_middlename,
        office_lastname: req.body.office_lastname,
        office_country_location: req.body.office_country_location,
        office_city_location: req.body.office_city_location,
        office_location: req.body.office_location,
        office_adress_street: req.body.office_adress_street,
        office_adress_number: req.body.office_adress_number,
        office_adress_postcode: req.body.office_adress_postcode,
        office_VAT_Num: req.body.office_VAT_Num,
        office_telephone: req.body.office_telephone,
        office_email: req.body.office_email,
        office_SoMe_One: req.body.office_SoMe_One,
        office_SoMe_Two: req.body.office_SoMe_Two,
        office_SoMe_Three: req.body.office_SoMe_Three,
        office_SoMe_Four: req.body.office_SoMe_Four,
        office_SoMe_Five: req.body.office_SoMe_Five,
        office_SoMe_Six: req.body.office_SoMe_Six,
        office_language: req.body.office_language,
 
            
    })

    console.log(response)
    res.send({ok: true, message: `${response.office_firstname} created in offices collection`})

    }catch(error){

        console.log(error)
        res.send({ok: false, message: `Something went wrong`})
    }
}
// //=====================================================

const remove = async (req, res) => {
    console.log("req:", req.params)
    try {
        console.log("req.params", req.params)
        const { _id } = req.params
        const response = await Offices.deleteOne({_id: _id})
        console.log('response', response)
        res.send({ok: true, message:`office with name: ${req.params.office_firstname}removed`})

    }catch(error){
        console.log('error inside Remove Catch of Server', error)
        res.send({ok: false, message:'Could not find unique id - ObjectId failed'})
    }
}

// // //=====================================================

const findoffices = async (req, res) => {
 
    try {
        const response = await Offices.find({})
        
        res.send({ok: true, response})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}

// // //=====================================================

const findOneoffice = async (req, res) => {
    const { _id, office_firstname } = req.body
    const value = _id || office_firstname
    const key = _id? "_id": "office_firstname"
    try {
        const response = await Offices.findOne({[key]:value})
        if(response === null) {
            console.log('response ====>', res)
            res.send({ok: false, message: `${value} does not exist`})
        }
        res.send({ok: true, response})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}
// // //=====================================================

const updateoffice = async (req, res) => {
    
   const { _id, 
    officeID,
    office_image,
    office_logo,
    office_firstname,
    office_middlename,
    office_lastname,
    office_country_location,
    office_city_location,
    office_location,
    office_adress_street,
    office_adress_number,
    office_adress_postcode,
    office_VAT_Num,
    office_telephone,
    office_email,
    office_SoMe_One,
    office_SoMe_Two,
    office_SoMe_Three,
    office_SoMe_Four,
    office_SoMe_Five,
    office_SoMe_Six,
    office_language,
        } = req.body
    try {
        const response = await Offices.updateOne({_id:_id}, 
            {
            officeID: officeID,
            office_image: office_image,
            office_logo: office_logo,
            office_firstname: office_firstname,
            office_middlename: office_middlename,
            office_lastname: office_lastname,
            office_country_location: office_country_location,
            office_city_location: office_city_location,
            office_location: office_location,
            office_adress_street: office_adress_street,
            office_adress_number: office_adress_number,
            office_adress_postcode: office_adress_postcode,
            office_VAT_Num: office_VAT_Num,
            office_telephone: office_telephone,
            office_email: office_email,
            office_SoMe_One: office_SoMe_One,
            office_SoMe_Two: office_SoMe_Two,
            office_SoMe_Three: office_SoMe_Three,
            office_SoMe_Four: office_SoMe_Four,
            office_SoMe_Five: office_SoMe_Five,
            office_SoMe_Six: office_SoMe_Six,
            office_language: office_language,
            })
        console.log(response)      
        res.send({ok: true, response, Updatedoffice:req.body})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}


module.exports = {
    add,
    remove,
    findoffices,
    findOneoffice,
    updateoffice

}