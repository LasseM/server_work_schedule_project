const Events = require('../models/events.js')

//=====================================================
const add = async (req, res) => {
    
    try{

        let eventDate = req.body.event_date
        console.log('req.body.event_date ===>>', req.body.event_date)

        console.log('eventDate ===>>', eventDate)
    const response = await Events.create({
        
        eventID: req.body.eventID,
        event_firstname: req.body.event_firstname,
        event_type: req.body.event_type,
        event_image: req.body.event_image,
        event_logo: req.body.event_logo,
        event_middlename: req.body.event_middlename,
        event_lastname: req.body.event_lastname,
        event_date: eventDate,
        event_startdate: req.body.event_startdate,
        event_starttime: req.body.event_starttime,
        event_enddate: req.body.event_enddate,
        event_endtime: req.body.event_endtime,
        event_country_location: req.body.event_country_location,
        event_city_location: req.body.event_city_location,
        event_location: req.body.event_location,
        event_adress_street: req.body.event_adress_street,
        event_adress_number: req.body.event_adress_number,
        event_adress_postcode: req.body.event_adress_postcode,
        event_adress_country: req.body.event_adress_country,
        event_staffneeded: req.body.event_staffneeded,
        event_jobtasks: req.body.event_jobtasks,
        event_type_of_staff_needed: req.body.event_type_of_staff_needed,
        event_staffsignedup: req.body.event_staffsignedup,
        event_language: req.body.event_language,
        event_general_notes_to_staff: req.body.event_general_notes_to_staff,
        event_dutymanagers: req.body.event_dutymanagers,
            
    })

    console.log(response)
    res.send({ok: true, message: `${response.event_firstname} created in events collection`})

    }catch(error){
        res.send({ok: false, message: error.message}) 
    }
}
// //=====================================================

const remove = async (req, res) => {
    console.log("req:", req.params)
    try {
        console.log("req.params", req.params)
        const { _id } = req.params
        const response = await Events.deleteOne({_id: _id})
        console.log("Respo:", response)
        res.send({ok: true, message:`Event with system id: >> ${_id} << has been removed`})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Could not find unique id - ObjectId failed'})
    }
}

// // //=====================================================

const findevents = async (req, res) => {
 
    try {
        const response = await Events.find({})
        
        res.send({ok: true, response})

    }catch(error){
       console.log("Error from Server=>", response)
        console.log('req', req)
        res.send({ok: false, message:'Something went wrong'})

        // Error 
        if (error.response) {
            /*
             * The request was made and the server responded with a
             * status code that falls out of the range of 2xx
             */
            console.log('1 - error.response.data', error.response.data);
            console.log('2 - error.response.status', error.response.status);
            console.log('3 - error.response.headers', error.response.headers);
        } else if (error.request) {
            /*
             * The request was made but no response was received, `error.request`
             * is an instance of XMLHttpRequest in the browser and an instance
             * of http.ClientRequest in Node.js
             */
            console.log('4 - error.request', error.request);
        } else {
            // Something happened in setting up the request and triggered an Error
            console.log('5 - Error', error.message);
        }
        console.log('6 - error', error);
    }
}

// // //=====================================================

const findOneevent = async (req, res) => {
   
    const { _id, event_firstname } = req.body
    const value = _id || event_firstname
    const key = _id? "_id": "event_firstname"
    try {
        const response = await Events.findOne({[key]:value})
        if(response === null) {
            res.send({ok: false, message: `${value} does not exist`})
        }
        res.send({ok: true, response})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}
// // //=====================================================

const updateevent = async (req, res) => {
    
   const { _id, 
    eventID,
    event_firstname,
    event_type,
    event_image,
    event_logo,
    event_middlename,
    event_lastname,
    event_date,
    event_startdate,
    event_starttime,
    event_enddate,
    event_endtime,
    event_country_location,
    event_city_location,
    event_location,
    event_adress_street,
    event_adress_number,
    event_adress_postcode,
    event_adress_country,
    event_staffneeded,
    event_jobtasks,
    event_type_of_staff_needed,
    event_staffsignedup,
    event_language,
    event_general_notes_to_staff,
    event_dutymanagers,
        } = req.body
    try {
        const response = await Events.updateOne({_id:_id}, 
            {
        eventID: eventID,
        event_firstname: event_firstname,
        event_type: event_type,
        event_image: event_image,
        event_logo: event_logo,
        event_middlename: event_middlename,
        event_lastname: event_lastname,
        event_date: event_date,
        event_startdate: event_startdate,
        event_starttime: event_starttime,
        event_enddate: event_enddate,
        event_endtime: event_endtime,
        event_country_location: event_country_location,
        event_city_location: event_city_location,
        event_location: event_location,
        event_adress_street: event_adress_street,
        event_adress_number: event_adress_number,
        event_adress_postcode: event_adress_postcode,
        event_adress_country: event_adress_country,
        event_staffneeded: event_staffneeded,
        event_jobtasks: event_jobtasks,
        event_type_of_staff_needed: event_type_of_staff_needed,
        event_staffsignedup: event_staffsignedup,
        event_language: event_language,
        event_general_notes_to_staff: event_general_notes_to_staff,
        event_dutymanagers: event_dutymanagers,
            })
        console.log(response)      
        res.send({ok: true, response, UpdatedEvent:req.body})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}


module.exports = {
    add,
    remove,
    findevents,
    findOneevent,
    updateevent

}