const Employees = require('../models/employees.js')
const config     = require('../config');
const saltRounds = 10;
const bcryptjs  = require('bcryptjs');
const jwt = require('jsonwebtoken')


//==============Verify======================

const verify_token = (req,res) => {

  
         const { token } = req.body;
         jwt.verify(token, config.secret, (err,succ) => {
            console.log("succ", succ)
               err ? res.json({ok:false,message:'something went wrong'}) 
               : res.json({ok:true,
                    userData: {
                        employeeID: succ.employeeID,
                        employee_type: succ.employee_type,
                        employee_image: succ.employee_image,
                        employee_firstname: succ.employee_firstname,
                        employee_middlename: succ.employee_middlename,
                        employee_lastname: succ.employee_lastname,
                        employee_email: succ.employee_email,
                        employee_addressStreet: succ.employee_addressStreet,
                        employee_addressNumber: succ.employee_addressNumber,
                        employee_postalCode: succ.employee_postalCode,
                        employee_city: succ.employee_city,
                        employee_country: succ.employee_country,
                        employee_phone: succ.employee_phone,
                        employee_mobile: succ.employee_mobile,
                        employee_office: succ.employee_office,
                        employee_position: succ.employee_position,
                        employee_focusarea: succ.employee_focusarea,
                        employee_nationality: succ.employee_nationality,
                        employee_language: succ.employee_language,
                        employee_bank_reg: succ.employee_bank_reg,
                        employee_bank_account_number: succ.employee_bank_account_number,
                        employee_payroll_num: succ.employee_payroll_num,
                        employee_tax_card: succ.employee_tax_card,
                    }
                
                })
         });      
  }
  
  
  //================Log-in=========================
  
  const login = async (req,res) => {
      console.log("req inside log-in ===>", req.body)
      const { employee_email , employee_password } = req.body;

  
      if( !employee_email || !employee_password ) res.json({ok:false,message:'All field are required'});
      try{
          const user = await Employees.findOne({ employee_email });
          if( !user ) return res.json({ok:false,message:'please provide a valid email'});
          const match = await bcryptjs.compare(employee_password, user.employee_password);
          console.log("match=>", match)
          if(match) {
              const token = jwt.sign(user.toJSON(), config.jwt_secret , { expiresIn:100080 });
              //  console.log("configSec", config)
              //  console.log("token", token)
              user.employee_password = ''
            //   console.log("user =>", user)
             res.json({ok:true,message:'welcome back', token, user}) 
          }else return res.json({ok:false,message:'invalid password'})
          
      }catch( error ){
           res.json({ok:false,error})
      }
  }


//=============Register=====================  

const register = async (req,res) => {
    const { 
        employeeID,
        employee_password,
        employee_password2,
        employee_type,
        employee_image,
        employee_firstname,
        employee_middlename,
        employee_lastname,
        employee_email,
        employee_addressStreet,
        employee_addressNumber,
        employee_postalCode,
        employee_city,
        employee_country,
        employee_phone,
        employee_mobile,
        employee_office,
        employee_position,
        employee_focusarea,
        employee_nationality,
        employee_language,
        employee_bank_reg,
        employee_bank_account_number,
        employee_payroll_num,
        employee_tax_card,
        } = req.body;
   console.log(req.body) 
	if( !employee_email || !employee_password || !employee_password2 || !employee_type || !employee_firstname || !employee_lastname) return res.json({ok:false,message:'Please fill in required fields'});
    if(  employee_password !== employee_password2) return res.json({ok:false,message:'passwords must match'});
    try{
    	const user = await Employees.findOne({ employee_email })
    	if( user ) return res.json({ok:false,message:'email already in use'});
    	const hash = await bcryptjs.hash(employee_password, saltRounds)
        console.log('hash =' , hash)
        const newUser = {
            employee_password : hash,
            employeeID: req.body.employeeID,     
            employee_type: req.body.employee_type,
            employee_image: req.body.employee_image,
            employee_firstname: req.body.employee_firstname,
            employee_middlename: req.body.employee_middlename,
            employee_lastname: req.body.employee_lastname,
            employee_email: req.body.employee_email,
            employee_addressStreet: req.body.employee_addressStreet,
            employee_addressNumber: req.body.employee_addressNumber,
            employee_postalCode: req.body.employee_postalCode,
            employee_city: req.body.employee_city,
            employee_country: req.body.employee_country,
            employee_phone: req.body.employee_phone,
            employee_mobile: req.body.employee_mobile,
            employee_office: req.body.employee_office,
            employee_position: req.body.employee_position,
            employee_focusarea: req.body.employee_focusarea,
            employee_nationality: req.body.employee_nationality,
            employee_language: req.body.employee_language,
            employee_bank_reg: req.body.employee_bank_reg,
            employee_bank_account_number: req.body.employee_bank_account_number,
            employee_payroll_num: req.body.employee_payroll_num,
            employee_tax_card: req.body.employee_tax_card,
        }    
        const create = await Employees.create(newUser)

        res.json({ok:true,message:'successful register'})
    }catch( error ){
        res.json({ok:false,error})
        console.log("some catch Error in the registersection")
    }    
}    


// //===================Remove Employees==============================

const remove = async (req, res) => {
    console.log("req:", req.params)
    try {
        console.log("req.params", req.params)
        const { _id } = req.params
        const response = await Employees.deleteOne({_id: _id})
        console.log("Respo:", response)
        res.send({ok: true, message:`Employee with system id: >> ${_id} << has been removed`})

        //  ${employee_lastname}

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Could not find unique id - ObjectId failed'})
    }
}

// // //=================== Find all Employees  ================================

const findemployees = async (req, res) => {
    
    try {
        const response = await Employees.find({})
        
        res.send({ok: true, response})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}

// // //=======================  Find One Employee  ==============================

// const findOneemployee = async (req, res) => {
   
//     const { _id, employee_firstname, employee_lastname } = req.body
   
//     const key
//     { 
//     if(_id){key = "_id" && value = _id} 
//     else if(employee_firstname){key = "employee_firstname" && value = employee_firstname}
//     else{key = 'employee_lastname' && value = employee_lastname} 
//     }
//     try {
//         const response = await Employees.findOne({[key]:value})
//         if(response === null) {
//             res.send({ok: false, message: `${value} does not exist`})
//         }
//         res.send({ok: true, response})

//     }catch(error){
//         console.log(error)
//         res.send({ok: false, message:'Something went wrong'})
//     }
// }

// const findOneemployee = async (req, res) => {
//     console.log('req.body', req.body)
//     const { _id, employee_firstname } = req.body

    // console.log('_id ==>', _id)
    // const value = _id || employee_firstname
    // const key = _id? "_id": "employee_firstname"
//     try {
//         const response = await Employees.findOne(req.body)
//         if(response === null) {
//             res.send({ok: false, message: `${value} does not exist`})
//         }
//         res.send({ok: true, response})
        
//     }catch(error){
//         console.log(error)
//         res.send({ok: false, message:'Something went wrong'})
//     }
// }


// ============== The functioning one ============ //
const findOneemployee = async (req, res) => {
    console.log('req.body', req.body)
    const { _id, employee_firstname } = req.body
    const value = _id || employee_firstname
    const key = _id? "_id": "employee_firstname"
    try {
        const response = await Employees.findOne({[key]:value})
        if(response === null) {
            res.send({ok: false, message: `${value} does not exist`})
        }
        res.send({ok: true, response})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}
// // //======================= Update One Employee  ==============================

const updateemployee = async (req, res) => {
   const { _id, 
    employeeID,
    // employee_password,
    // employee_password2,
    employee_type,
    employee_image,
    employee_firstname,
    employee_middlename,
    employee_lastname,
    employee_email,
    employee_addressStreet,
    employee_addressNumber,
    employee_postalCode,
    employee_city,
    employee_country,
    employee_phone,
    employee_mobile,
    employee_office,
    employee_position,
    employee_focusarea,
    employee_nationality,
    employee_language,
    employee_bank_reg,
    employee_bank_account_number,
    employee_payroll_num,
    employee_tax_card,
        } = req.body
    try {
        const response = await Employees.updateOne({_id:_id}, 
            {
        employeeID:employeeID,
        // employee_password:employee_password,
        // employee_password:employee_password2,
        employee_type:employee_type,
        employee_image:employee_image,
        employee_firstname:employee_firstname,
        employee_middlename:employee_middlename,
        employee_lastname:employee_lastname,
        employee_email:employee_email,
        employee_addressStreet:employee_addressStreet,
        employee_addressNumber:employee_addressNumber,
        employee_postalCode:employee_postalCode,
        employee_city:employee_city,
        employee_country:employee_country,
        employee_phone:employee_phone,
        employee_mobile:employee_mobile,
        employee_office:employee_office,
        employee_position:employee_position,
        employee_focusarea:employee_focusarea,
        employee_nationality:employee_nationality,
        employee_language:employee_language,
        employee_bank_reg:employee_bank_reg,
        employee_bank_account_number:employee_bank_account_number,
        employee_payroll_num:employee_payroll_num,
        employee_tax_card:employee_tax_card,
            })
        console.log(response)      
        res.send({ok: true, message: `Employee profile with name ${employee_firstname} ${employee_lastname} has been updated`})

    }catch(error){
        console.log(error)
        res.send({ok: false, message:'Something went wrong'})
    }
}


module.exports = {
    remove,
    findemployees,
    findOneemployee,
    updateemployee,
    register, 
    login, 
    verify_token

}