const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const employeeSchema = new Schema({
    employeeID:{
        index: { unique: true },
        type: String,
    },
    employee_password:{
        required: true,
        type: String,
    },
    employee_type:{
        required: true,
        type: String,
    },        
    employee_image:{
        type: Object,
    },
    employee_firstname:{
        type: String,
        required: true,
    },
    employee_middlename:{
        type: String,
    },
    employee_lastname:{
        type: String,
        required: true,
    },
    employee_email:{
        type: String,
    },
    employee_addressStreet:{
        type: String,
    },
    employee_addressNumber:{
        type: String,
    },
    employee_postalCode:{
        type: String,
    },
    employee_city:{
        type: String,
    },
    employee_country:{
        type: String,
    },
    employee_phone:{
        type: String,
    },
    employee_mobile:{
        type: String,
    },
    employee_office:{
        type: String,
    },
    employee_position:{
        type: String,
    },
    employee_focusarea:{
        type: String,
    },
    employee_nationality:{
        type: String,
    },
    employee_language:{
        type: String,
    },
    employee_bank_reg:{
        type: String
    },
    employee_bank_account_number:{
        type: String
    },
    employee_payroll_num:{
        type: String
    },
    employee_tax_card:{
        type: String
    },
    

})

module.exports =  mongoose.model('employees', employeeSchema);