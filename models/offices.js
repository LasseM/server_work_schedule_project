const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const officeSchema = new Schema({
    officeID:{
        index: { unique: true },
        type: String,
    },
    office_image:{
        type: Object,
    },
    office_logo:{
        type: Object,
    },
    office_firstname:{
        type: String,
        required: true,
    },
    office_middlename:{
        type: String,
    },
    office_lastname:{
        type: String,
    },
    office_country_location:{
        type: String,
        required: true,
    },
    office_city_location:{
        type: String,
        required: true,
    },
    office_location:{
        type: String,
    },
    office_adress_street:{
        type: String,
    },
    office_adress_number:{
        type: String,
    },
    office_adress_postcode:{
        type: String,
    },
    office_VAT_Num:{
        type: String,
    },
    office_telephone:{
        type: String,
    },
    office_email:{
        type: String,
    },
    office_SoMe_One:{
        type: String,
    },
    office_SoMe_Two:{
        type: String,
    },
    office_SoMe_Three:{
        type: String,
    },
    office_SoMe_Four:{
        type: String,
    },
    office_SoMe_Five:{
        type: String,
    },
    office_SoMe_Six:{
        type: String,
    },
    office_language:{
        type: Array,
    },


})

module.exports =  mongoose.model('offices', officeSchema);