const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const eventSchema = new Schema({
    eventID:{
        index: { unique: true },
        type: String,
    },
    event_type:{
        type: String,
    },
    event_image:{
        type: Object,
    },
    event_logo:{
        type: Object,
    },
    event_firstname:{
        type: String,
        required: true,
    },
    event_middlename:{
        type: String,
    },
    event_lastname:{
        type: String,
    },
    event_date:{
        type: Date,
        required: true,
    },
    event_startdate:{
        type: String,
    },
    event_starttime:{
        type: String,
    },
    event_enddate:{
        type: String,
    },
    event_endtime:{
        type: String,
    },
    event_country_location:{
        type: String,
        required: true,
    },
    event_city_location:{
        type: String,
        required: true,
    },
    event_location:{
        type: String,
    },
    event_adress_street:{
        type: String,
    },
    event_adress_number:{
        type: String,
    },
    event_adress_postcode:{
        type: String,
    },
    event_adress_country:{
        type: String,
    },
    event_staffneeded:{
        type: String,
    },
    event_jobtasks:{
        type: Array,
    },
    event_type_of_staff_needed:{
        type: Array,
    },
    event_staffsignedup:{
        type: Array,

    },
    event_language:{
        type: Array,
    },
    event_general_notes_to_staff:{
        type: String,
    },
    event_dutymanagers:{
        type: Array,
    },

})

module.exports =  mongoose.model('events', eventSchema);