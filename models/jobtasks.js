const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const jobtaskSchema = new Schema({
    jobtaskID:{
        index: { unique: true },
        type: String,
    },
    jobtask_type:{
        type: String,
    },
    jobtask_image:{
        type: Object,
    },
    jobtask_logo:{
        type: Object,
    },
    jobtask_firstname:{
        type: String,
        required: true,
    },
    jobtask_middlename:{
        type: String,
    },
    jobtask_lastname:{
        type: String,
    },
    jobtask_date:{
        type: String,
        required: true,
    },
    jobtask_startdate:{
        type: String,
    },
    jobtask_starttime:{
        type: String,
    },
    jobtask_enddate:{
        type: String,
    },
    jobtask_endtime:{
        type: String,
    },
    jobtask_country_location:{
        type: String,
        required: true,
    },
    jobtask_city_location:{
        type: String,
        required: true,
    },
    jobtask_location:{
        type: String,
    },
    jobtask_adress_street:{
        type: String,
    },
    jobtask_adress_number:{
        type: String,
    },
    jobtask_adress_postcode:{
        type: String,
    },
    jobtask_adress_country:{
        type: String,
    },
    jobtask_confirmed:{
        type: Boolean,
    },
    jobtask_language:{
        type: Array,
    },
    jobtask_general_notes_to_staff:{
        type: String,
    },
    jobtask_dutymanagers:{
        type: Object,
    },

})

module.exports =  mongoose.model('jobtasks', jobtaskSchema);