const router = require('express').Router()
const controller = require('../controllers/offices')


router.post('/add', controller.add)
router.delete('/remove/:_id', controller.remove)
router.get('/find_offices', controller.findoffices)
router.post('/find_one_office', controller.findOneoffice)
router.put('/update_office', controller.updateoffice)

module.exports = router