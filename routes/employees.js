const router = require('express').Router()
const controller = require('../controllers/employees')


router.post('/register', controller.register)
router.delete('/remove/:_id', controller.remove)
router.get('/find_employees', controller.findemployees)
router.post('/find_one_employee/', controller.findOneemployee)
router.put('/update_employee', controller.updateemployee)
router.post('/login',controller.login);
router.post('/verify_token',controller.verify_token);



module.exports = router