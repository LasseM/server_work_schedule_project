const router = require('express').Router()
const controller = require('../controllers/events.js')


router.post('/add', controller.add)
router.delete('/remove/:_id', controller.remove)
router.get('/find_events', controller.findevents)
router.post('/find_one_event', controller.findOneevent)
router.put('/update_event', controller.updateevent)


module.exports = router