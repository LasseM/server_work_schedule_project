const router = require('express').Router()
const controller = require('../controllers/jobtasks')


router.post('/add', controller.add)
router.delete('/remove/:_id', controller.remove)
router.get('/find_jobtasks', controller.findjobtasks)
router.post('/find_one_jobtask', controller.findOnejobtask)
router.put('/update_jobtask', controller.updatejobtask)

module.exports = router