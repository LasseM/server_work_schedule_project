const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser');
const port = process.env.PORT || 3040
const config = require('./config.js')
const cors = require('cors')
app.use(cors())

// cloudinary 				= require('cloudinary'),
// cloudinaryConfig    	= require('./configs');

// =================== initial settings ===================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// connnect to mongo

// connecting to mongo and checking if DB is running
async function connecting(){
try {       
    await mongoose.connect(`mongodb+srv://LasseM:${config.mlab_pass}@cluster0-xy33k.mongodb.net/work_schedule_project?retryWrites=true&w=majority`, { useUnifiedTopology: true , useNewUrlParser: true })
    console.log('Connected to the DB')
} catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up !!!');
}
}
connecting()
// temp stuff to suppress internal warning of mongoose which would be updated by them soon
mongoose.set('useCreateIndex', true);
// end of connecting to mongo and checking if DB is running

//Server route
app.use('/emails', require('./routes/emails.js'))

// routes
app.use('/employees', require('./routes/employees.js'));
app.use('/events', require('./routes/events.js'));
app.use('/jobtasks', require('./routes/jobtasks.js'));
app.use('/offices', require('./routes/offices.js'));

// Set the server to listen on port 3040
app.listen(port, () => console.log(`listening on port 3040`))